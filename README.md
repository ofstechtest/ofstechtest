#  OFSTechTest

## Requirements

1. Quickly implement an API (at least 2 endpoints)
2. Use Moya
3. Use Codable
4. Design a basic UI
5. Include unit tests

## Accomplishments

- 2 endpoints are called (`GET /projects` & `GET /projects/:id`)
- Moya is used
- Codable is used
- Very basic UI

## Explanations

I used the [Behance api](https://www.behance.net/dev) for this test. The features of the app are a Behance projects' feed and a detail of
those project.

Firstly I think I didn't quite use Moya the way it is supposed to be used. I was not comfortable with the way to group all kinds of requests
in the same provider. It is a drawback for me as, in my opinion, it is not very scalable and does not enforce a right separation of concern.
Furthermore I find it very redoundant to parse json each time you get a response from the webservices. In addition, I found that Moya
is a framework that leak easily. So I used Moya in a way that my not be what you expected (`TypedMoyaProvider` which is a subclass
of `MoyaProvider`)  but which was, as always in my opinion, clearer (the strength of Moya is the `TargetType`) and more usable, I only
parse json once. I reckon that all I have done might come from a very bad understanding of the Moya's documentation and my first time
with this framework.

I used `Kingfisher` to handle image download, it was my first time using it also. I encapsulate it to easily change it if needed.

For the architecture I used an MVC, that could, unfortunately, easily become a Massive View Controller. To avoid that I would have separated
the UI components inside their own `UIView` and only integrate those `UIView` inside the `UIViewController`. Time was the main factor
about why I did not implement it.

I separated the `Project` api object in 2 sub-objects (`ProjectFeed` and `ProjectDetail`). Those contain only the required attributs for
the module they are in. To my mind, it is more scalable as it allows to completely split those totally different modules.

Concerning the UI I used a collection view for the feed. It was not necessary for that exercice, but is is much more customisable that a
table view (add custom cells -related projects, friends...-). The detail view is done with a table view. It was easier to put the project's elements
that way.

I didn't had the time to include unit tests , but thanks to Moya the network part and parsing could have been easily checked.
