//
//  ProjectDetail.swift
//  OFSTechTest
//
//  Created by Paul-Emmanuel on 18/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import Foundation

/// Used to parse the json more easily
struct ProjectDetailResponse: Codable {
    let project: ProjectDetail
}

struct ProjectDetail: Codable {
    /// The identifier used to get the Project detail.
    let id: Int

    /// The name to be displayed.
    let name: String

    /// The covers, only the first one will be used.
    let covers: [String: String]

    /// Project description.
    let description: String

    let stats: ProjectDetailStats
}

struct ProjectDetailStats: Codable {
    let views: Int
    let appreciations: Int
    let comments: Int
}
