//
//  ProjectDetailService.swift
//  OFSTechTest
//
//  Created by Paul-Emmanuel on 18/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import Foundation
import Moya

enum ProjectDetailService {
    init() {
        self = .project(id: -1)
    }

    case project(id: Int)
}

extension ProjectDetailService: TargetType {
    var baseURL: URL { return URL(string: "https://api.behance.net/v2/") ?? URL(target: ProjectDetailService()) }
    
    var path: String {
        switch self {
        case .project(let id):
            return "projects/\(id)"
        }
    }

    var method: Moya.Method { return .get }

    var sampleData: Data { return Data() }

    var task: Task { return .requestParameters(parameters: ["api_key": "PD3lBAQNUFqGpI14pcJI6CkFb3dDYwo1"], encoding: URLEncoding.queryString) }

    var headers: [String : String]? { return nil }
}
