//
//  ProjectDetailStatsTableViewCell.swift
//  OFSTechTest
//
//  Created by Paul Emmanuel Garcia on 18/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import UIKit

class ProjectDetailStatsTableViewCell: UITableViewCell {
    @IBOutlet private weak var nbViewsLabel: UILabel!
    @IBOutlet private weak var nbAppreciationsLabel: UILabel!
    @IBOutlet private weak var nbCommentsLabel: UILabel!
}

extension ProjectDetailStatsTableViewCell: ProjectDetailCell {
    func configure(with projectDetail: ProjectDetail) {
        nbViewsLabel.text = "\(projectDetail.stats.views) 👀"
        nbAppreciationsLabel.text = "\(projectDetail.stats.appreciations) ❤️"
        nbCommentsLabel.text = "\(projectDetail.stats.comments) 📝"
    }
}
