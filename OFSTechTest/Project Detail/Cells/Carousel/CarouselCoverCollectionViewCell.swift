//
//  CarouselCoverCollectionViewCell.swift
//  OFSTechTest
//
//  Created by Paul Emmanuel Garcia on 18/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import UIKit

class CarouselCoverCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!

    override func prepareForReuse() {
        super.prepareForReuse()

        coverImageView.image = nil
    }

    func configure(with imageUrl: URL) {
        coverImageView.setImage(with: imageUrl)
    }
}
