//
//  ProjectDetailCarouselTableViewCell.swift
//  OFSTechTest
//
//  Created by Paul Emmanuel Garcia on 18/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import UIKit

class ProjectDetailCarouselTableViewCell: UITableViewCell {

    @IBOutlet private weak var carouselCollectionView: UICollectionView!
    var picturesUrl: [URL] = []
}

extension ProjectDetailCarouselTableViewCell: ProjectDetailCell {
    func configure(with projectDetail: ProjectDetail) {
        picturesUrl = projectDetail.covers.values.flatMap { URL(string: $0) }
        carouselCollectionView.reloadData()
    }
}

extension ProjectDetailCarouselTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picturesUrl.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let coverCell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "ProjectDetailCarouselCover",
                                 for: indexPath) as? CarouselCoverCollectionViewCell else {
                                    return UICollectionViewCell()
        }

        coverCell.configure(with: picturesUrl[indexPath.item])

        return coverCell
    }


}

