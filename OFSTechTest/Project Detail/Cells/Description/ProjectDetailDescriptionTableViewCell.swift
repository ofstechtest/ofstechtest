//
//  ProjectDetailDescriptionTableViewCell.swift
//  OFSTechTest
//
//  Created by Paul Emmanuel Garcia on 18/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import UIKit

class ProjectDetailDescriptionTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!

}

extension ProjectDetailDescriptionTableViewCell: ProjectDetailCell {
    func configure(with projectDetail: ProjectDetail) {
        nameLabel.text = projectDetail.name
        descriptionLabel.text = projectDetail.description
    }
}
