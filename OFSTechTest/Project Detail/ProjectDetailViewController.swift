//
//  ProjectDetailViewController.swift
//  OFSTechTest
//
//  Created by Paul-Emmanuel on 18/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import UIKit

protocol ProjectDetailCell {
    func configure(with projectDetail: ProjectDetail)
}

class ProjectDetailViewController: UIViewController {

    let projectDetailProvider = TypedMoyaProvider<ProjectDetailService, ProjectDetailResponse>()
    var id: Int = -1
    var detail: ProjectDetail?
    let cellsIdentifier = ["ProjectDetailCarousel", "ProjectDetailDescription", "ProjectDetailStats"]
    @IBOutlet private weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        tableView.tableFooterView = UIView()

        projectDetailProvider.typedRequest(target: .project(id: id),
                                           success: { [weak self] projectDetailResponse in
                                            guard let strongSelf = self else { return }
                                            strongSelf.detail = projectDetailResponse.project
                                            strongSelf.tableView.reloadData()
                                            strongSelf.title = projectDetailResponse.project.name
        },
                                           failure: { _ in

        })
    }

}

extension ProjectDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsIdentifier.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellsIdentifier[indexPath.row]) else { return UITableViewCell() }

        detail.map { (cell as? ProjectDetailCell)?.configure(with: $0) }

        return cell
    }
}
