//
//  FeedService.swift
//  OFSTechTest
//
//  Created by Paul-Emmanuel on 17/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import Foundation
import Moya

enum FeedService {

    init() {
        self = .projects(query: nil, sort: nil)
    }

    enum SortParameters: String {
        case appreciations
        case views
        case comments
    }

    case projects(query: String?, sort: SortParameters?)
}

extension FeedService: TargetType {

    var baseURL: URL { return URL(string: "https://api.behance.net/v2/") ?? URL(target: FeedService()) }

    var path: String {
        switch self {
        case .projects:
            return "projects"
        }
    }

    var method: Moya.Method {
        return .get
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .projects(let query, let sort):
            var parameters: [String: String] = ["api_key": "PD3lBAQNUFqGpI14pcJI6CkFb3dDYwo1"]

            query.map { parameters["q"] = $0 }
            sort.map { parameters["sort"] = $0.rawValue }

            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
    }

    var headers: [String : String]? {
        return nil
    }

}
