//
//  ProjectCollectionViewCell.swift
//  OFSTechTest
//
//  Created by Paul-Emmanuel on 18/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import UIKit

class ProjectCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainPictureImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()

        mainPictureImageView.isHidden = false
        mainPictureImageView.image = nil
    }

    func configure(with project: FeedProject) {
        if let url = project.covers.values.first.flatMap({ URL(string: $0) }) {
            mainPictureImageView.setImage(with: url)
        } else {
            mainPictureImageView.isHidden = true
        }

        nameLabel.text = project.name
    }
}
