//
//  FeedViewController.swift
//  OFSTechTest
//
//  Created by Paul-Emmanuel on 17/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {

    @IBOutlet private weak var collectionView: UICollectionView!
    let projectsProvider = TypedMoyaProvider<FeedService, FeedProjects>()
    var projects: [FeedProject] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Projects Feed"

        projectsProvider.typedRequest(target: FeedService.projects(query: nil, sort: nil),
                                      success: { [weak self] projects in
                                        guard let strongSelf = self else { return }

                                        strongSelf.projects = projects.projects
                                        strongSelf.collectionView.reloadData()
            },
                                      failure: { _ in
                                        print("Failure")
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            segue.identifier == Segue.detail.rawValue,
            let feedProject = sender as? FeedProject
            else { return }

        (segue.destination as? ProjectDetailViewController)?.id = feedProject.id
    }
}

extension FeedViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return projects.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProjectCell", for: indexPath) as? ProjectCollectionViewCell else { return UICollectionViewCell() }

        cell.configure(with: projects[indexPath.item])

        return cell
    }
}

extension FeedViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: Segue.detail.rawValue, sender: projects[indexPath.item])
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 20, height: view.frame.width - 20)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
