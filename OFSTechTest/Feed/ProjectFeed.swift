//
//  ProjectFeed.swift
//  OFSTechTest
//
//  Created by Paul-Emmanuel on 17/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import Foundation


/// Used to parse more easily the json.
struct FeedProjects: Codable {
    let projects: [FeedProject]
}

/// FeedProject that holds the basic information needed for the feed.
struct FeedProject: Codable {

    /// The identifier used to get the Project detail
    let id: Int

    /// The name to be displayed
    let name: String

    /// The covers, only the first one will be used.
    let covers: [String: String]
}
