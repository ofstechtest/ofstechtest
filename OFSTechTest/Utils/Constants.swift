//
//  Constants.swift
//  OFSTechTest
//
//  Created by Paul-Emmanuel on 17/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import Foundation

enum Segue: String {
    case detail = "ProjectDetail"
}
