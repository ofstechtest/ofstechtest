//
//  Moya+OFSTechTest.swift
//  OFSTechTest
//
//  Created by Paul-Emmanuel on 17/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import Foundation
import Moya

/// MoyaProvider subclass that allows to have a typed response from the webservices.
/// It uses the Moya `TargetType` to build the request.
class TypedMoyaProvider<Target: TargetType, ResultType>: MoyaProvider<Target> where ResultType: Codable {

    /// Custom error.
    ///
    /// - codable: Send when the decoder failed to decode the json.
    enum ParsingError: Error {
        case decoder
    }

    /// Handle a webservice call and response.
    /// You handle a typed instance in the success completion block
    /// or an `Error` when the request failed.
    ///
    /// - Parameters:
    ///   - target: The TargetType from Moya to build the request.
    ///   - success: The mandatory success completion block.
    ///   - failure: The optional failure completion block.
    /// - Returns: A cancellable request.
    @discardableResult
    func typedRequest(target: Target, success: @escaping (ResultType) -> (), failure: ((Error) -> ())? = nil) -> Cancellable {
        return request(target) { result in
            switch result {
            case let .success(response):
                let decoder = JSONDecoder()
                guard
                let json = try? response.mapJSON(),
                    let object = try? decoder.decode(ResultType.self, from: JSONSerialization.data(withJSONObject: json)) else {
                        failure?(ParsingError.decoder)
                        return
                }
                success(object)
            case let .failure(error):
                failure?(error)
            }

        }
    }

}

