//
//  UIImageView+OFSTechTest.swift
//  OFSTechTest
//
//  Created by Paul-Emmanuel on 17/12/2017.
//  Copyright © 2017 rstudio. All rights reserved.
//

import UIKit
import Kingfisher

/// View used as a loading indicator view for Kingfisher
class LoadingImageView: UIView {

    override func didMoveToSuperview() {
        guard let superview = superview else { return }

        backgroundColor = .lightGray
        translatesAutoresizingMaskIntoConstraints = false

        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.startAnimating()

        NSLayoutConstraint.activate([
            // LoadingImageView Constraints
            leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: 0),
            topAnchor.constraint(equalTo: superview.topAnchor, constant: 0),
            trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: 0),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 0),

            // Activity Indicator Constraints
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0),
            activityIndicator.widthAnchor.constraint(equalToConstant: 15),
            activityIndicator.heightAnchor.constraint(equalToConstant: 15)
            ])
    }
}

extension LoadingImageView: Placeholder { }

extension UIImageView {
    func setImage(with url: URL) {
        kf.setImage(with: url, placeholder: LoadingImageView())
    }
}
